export const searchTripElements = {
    privacyPopup: '.cookie-popup-with-overlay__box',
    agreeButton: '.cookie-popup-with-overlay__button',
    departureField: '#input-button__departure',
    lisbon: '[data-id="LIS"]',
    destinationField: '#input-button__destination',
    parisBeauvais: '[data-id="BVA"]',
    departDate: '[uniqueid="dates-from"]',
    months:'.m-toggle__month',
    departDateID: '[data-id="2021-09-30"]',
    nextMonth: '[data-ref="calendar-btn-next-month"]',
    returnDate: '[uniqueid="dates-to"]',
    returnDateID: '[data-id="2021-10-30"]',
    passengersAdults: '[data-ref="passengers-picker__adults"] [data-ref="counter.counter__increment"]',
    passengersChild: '[data-ref="passengers-picker__children"] [data-ref="counter.counter__increment"]',
    searchButton: '.flight-search-widget__start-search-container .flight-search-widget__start-search'

}

export const selectDates = {
    tarifaValue: '.price-wrapper',
    fareCard: '.fare-card.fare-card--primary',
    continueWithValue: 'fare-upgrade-footer-continue_button.ry-button--outline-light-blue.ry-button--full'
}

export const fillInformation = {
    logInLater: '.login-touchpoint__login-later.h3',
    title: '.dropdown.b2',
    namePassenger1: '#formState.passengers.ADT-0.name',
    lastNamePassenger1: 'formState.passengers.ADT-0.surname',
    namePassenger2: '#formState.passengers.ADT-1.name',
    lastNamePassenger2: 'formState.passengers.ADT-1.surname',
    namePassenger3: 'formState.passengers.CHD-0.name',
    lastNamePassenger3: 'formState.passengers.CHD-0.surname',
    continueButton: '.continue-flow__button.ry-button--gradient-yellow',
    okayButton: '.seats-modal__cta.ry-button--gradient-blue',
    seat1: '[data-ref="seat-map__seat-18A-selected"]', 
    seat2: '[data-ref="seat-map__seat-18B-selected"]',
    seat3: '[data-ref="seat-map__seat-18C-selected"]',
    nextFlyButton: '[data-ref="seats-action__button-next"]',
    noThanksLink: '.enhanced-takeover-beta__product-dismiss-cta.ry-button--anchor-blue.ry-button--anchor',
    smallPackage: '[for="ry-radio-button--0"]',
    continueButton2: 'ry-button--gradient-yellow',
    continueButton3: '.ry-button--full.ng-tns-c167-1.ry-button--gradient-yellow.ry-button--large', 
    stopTest: '.section-header__title.section-header__title--desktop'  
}