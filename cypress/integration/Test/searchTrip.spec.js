/// <reference types="Cypress" />
import { searchTripElements,
        selectDates,
        fillInformation
} from '../../pageLocators/searchTrip.locators'


describe ('Search a Trip',  () => {

    it('Search a Trip',  () => { 
    cy.fixture('passengerDetails.json').then(data => { 
        cy.visit('/')
        cy.get(searchTripElements.privacyPopup).should('be.visible').and('contain', 'We value your privacy')
        cy.get(searchTripElements.agreeButton).should('be.visible').click()
        cy.get(searchTripElements.departureField).should('be.visible').clear().type('Lisbon')
        cy.get(searchTripElements.lisbon).should('be.visible').click()
        cy.get(searchTripElements.destinationField).should('be.visible').clear().type('Paris Beauvais')
        cy.get(searchTripElements.parisBeauvais).should('be.visible').click()
        cy.get(searchTripElements.departDate).should('be.visible').click()
        cy.get(searchTripElements.months).should('be.visible').and('contain', 'Sep')
        cy.get(searchTripElements.departDateID).should('be.visible').click()           
        cy.get(searchTripElements.returnDateID).should('be.visible').click()
        cy.get(searchTripElements.passengersAdults).should('be.visible').click({ force: true })
        cy.get(searchTripElements.passengersChild).should('be.visible').click({ force: true })
        cy.get(searchTripElements.searchButton).should('be.visible').click()
        cy.get(selectDates.tarifaValue).should('be.visible').eq(0).click({ force: true })
        cy.get(selectDates.tarifaValue).should('be.visible').eq(0).click({ force: true })
        cy.get(selectDates.fareCard).should('be.visible').click({ force: true })
        cy.get(selectDates.continueWithValue).should('contain', 'Continue with Value fare').click({ force: true })
        cy.get(fillInformation.logInLater).should('be.visible').eq(1).click()
        cy.get(fillInformation.title).should('be.visible').eq(0).click().select('Ms')        
        cy.get(fillInformation.namePassenger1).should('be.visible').type(data.namePassenger1)   
        cy.get(fillInformation.lastNamePassenger1).should('be.visible').type(data.lastNamePassenger1)   
        cy.get(fillInformation.title).should('be.visible').eq(0).click().select('Mr')        
        cy.get(fillInformation.namePassenger2).should('be.visible').type(data.namePassenger2)   
        cy.get(fillInformation.lastNamePassenger2).should('be.visible').type(data.lastNamePassenger2)  
        cy.get(fillInformation.namePassenger3).should('be.visible').type(data.namePassenger3)   
        cy.get(fillInformation.lastNamePassenger3).should('be.visible').type(data.lastNamePassenger3) 
        cy.get(fillInformation.continueButton).should('be.visible').click()   
        cy.get(fillInformation.okayButton).should('be.visible').click()   
        cy.get(fillInformation.seat1).should('be.visible').click({ force: true })
        cy.get(fillInformation.seat2).should('be.visible').click({ force: true }) 
        cy.get(fillInformation.seat3).should('be.visible').click({ force: true })
        cy.get(fillInformation.nextFlyButton).should('be.visible').click({ force: true })
        cy.get(fillInformation.seat1).should('be.visible').click({ force: true })
        cy.get(fillInformation.seat2).should('be.visible').click({ force: true }) 
        cy.get(fillInformation.seat3).should('be.visible').click({ force: true })
        cy.get(fillInformation.noThanksLink).should('be.visible').click({ force: true })
        cy.get(fillInformation.smallPackage).should('be.visible').first().check()
        cy.get(fillInformation.continueButton2).should('contain', 'Continue').click({ force: true })
        cy.get(fillInformation.continueButton3).should('contain', 'Continue').click({ force: true })
        cy.get(fillInformation.stopTest).should('contain', 'Plan your whole trip')
        })
  
    })
    
})
                
